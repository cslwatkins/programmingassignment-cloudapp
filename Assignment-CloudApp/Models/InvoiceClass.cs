﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment_CloudApp.Models
{
    [Table("Invoices")]
    public class InvoiceClass
    {
        public int Id { get; set; }
        [Display(Name = "Customer Name")]
        public string customername { get; set; }
        [Display(Name = "Customer Address")]
        public string customeraddress { get; set; }
        [Display(Name = "Date")]
        public string date { get; set; }
        [Display(Name = "Invoice No.")]
        public int invoiceno { get; set; }
        [Display(Name = "Description")]
        public string description { get; set; }
        [Display(Name = "Invoice Total (in pence)")]
        public int invoicetotal { get; set; }
    }
}
