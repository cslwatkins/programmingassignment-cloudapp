﻿-- Script Date: 19/02/2024 11:57  - ErikEJ.SqlCeScripting version 3.5.2.95
CREATE TABLE [Invoices] (
  [Id] INTEGER PRIMARY KEY IDENTITY (1,1) NOT NULL
, [customername] TEXT NOT NULL
, [customeraddress] TEXT NOT NULL
, [date] TEXT NOT NULL
, [invoiceno] INTEGER NOT NULL
, [description] TEXT NOT NULL
, [invoicetotal] INTEGER NOT NULL
);