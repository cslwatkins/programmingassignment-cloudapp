﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Assignment_CloudApp.Data;
using Assignment_CloudApp.Models;

namespace Assignment_CloudApp.Pages.Invoices
{
    public class EditModel : PageModel
    {
        private readonly Assignment_CloudApp.Data.Assignment_CloudAppContext _context;

        public EditModel(Assignment_CloudApp.Data.Assignment_CloudAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InvoiceClass InvoiceClass { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceclass =  await _context.InvoiceClass.FirstOrDefaultAsync(m => m.Id == id);
            if (invoiceclass == null)
            {
                return NotFound();
            }
            InvoiceClass = invoiceclass;
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(InvoiceClass).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceClassExists(InvoiceClass.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool InvoiceClassExists(int id)
        {
            return _context.InvoiceClass.Any(e => e.Id == id);
        }
    }
}
