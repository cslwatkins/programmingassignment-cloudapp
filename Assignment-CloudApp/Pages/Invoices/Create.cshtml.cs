﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Assignment_CloudApp.Data;
using Assignment_CloudApp.Models;

namespace Assignment_CloudApp.Pages.Invoices
{
    public class CreateModel : PageModel
    {
        private readonly Assignment_CloudApp.Data.Assignment_CloudAppContext _context;

        public CreateModel(Assignment_CloudApp.Data.Assignment_CloudAppContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public InvoiceClass InvoiceClass { get; set; } = default!;

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.InvoiceClass.Add(InvoiceClass);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
