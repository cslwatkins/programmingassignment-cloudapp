﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Assignment_CloudApp.Data;
using Assignment_CloudApp.Models;

namespace Assignment_CloudApp.Pages.Invoices
{
    public class IndexModel : PageModel
    {
        private readonly Assignment_CloudApp.Data.Assignment_CloudAppContext _context;

        public IndexModel(Assignment_CloudApp.Data.Assignment_CloudAppContext context)
        {
            _context = context;
        }

        public IList<InvoiceClass> InvoiceClass { get;set; } = default!;

        public async Task OnGetAsync()
        {
            InvoiceClass = await _context.InvoiceClass.ToListAsync();
        }
    }
}
