﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Assignment_CloudApp.Data;
using Assignment_CloudApp.Models;

namespace Assignment_CloudApp.Pages.Invoices
{
    public class DeleteModel : PageModel
    {
        private readonly Assignment_CloudApp.Data.Assignment_CloudAppContext _context;

        public DeleteModel(Assignment_CloudApp.Data.Assignment_CloudAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public InvoiceClass InvoiceClass { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceclass = await _context.InvoiceClass.FirstOrDefaultAsync(m => m.Id == id);

            if (invoiceclass == null)
            {
                return NotFound();
            }
            else
            {
                InvoiceClass = invoiceclass;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var invoiceclass = await _context.InvoiceClass.FindAsync(id);
            if (invoiceclass != null)
            {
                InvoiceClass = invoiceclass;
                _context.InvoiceClass.Remove(InvoiceClass);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
