﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Assignment_CloudApp.Models;

namespace Assignment_CloudApp.Data
{
    public class Assignment_CloudAppContext : DbContext
    {
        public Assignment_CloudAppContext (DbContextOptions<Assignment_CloudAppContext> options)
            : base(options)
        {
        }

        public DbSet<Assignment_CloudApp.Models.InvoiceClass> InvoiceClass { get; set; } = default!;
    }
}
